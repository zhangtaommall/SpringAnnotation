# Spring注解驱动开发

## 核心容器IOC

### 如何向Spring中添加Bean实例

#### 1.通过@Bean手动添加

- 默认使用方法名为实例id
- 使用@Bean("XXX")指定实例id

```
/**
 * 向容器中注册一个Bean实例；类型是返回值的类型；
 * id默认是方法名字,或者使用@Bean("XXX")指定id名称
 */
@Bean("us")
public User user(){
    return new User("张凯",29);
}
```

#### 2.通过包扫描添加Bean实例@ComponentScan

- 注解：@ComponentScan

  使用属性basePackages指定包扫描范围

  ```
  @ComponentScan(basePackages = {"com.candy"})
  ```

- 排除扫描：excludeFilters属性(一般不与includeFilters同时使用)

  ```
  @ComponentScan.Filter(type = FilterType.ANNOTATION)
  ```

  FilterType.ANNOTATION：按注解方式匹配

  ```
  @ComponentScan(basePackages = {"com.candy"},excludeFilters = {
          @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = {Controller.class, Service.class})
  })
  ```

  FilterType.ASPECTJ：使用ASPECTJ表达式

  FilterType.ASSIGNABLE_TYPE：类型匹配，包含其子类，父类

  ```
  @ComponentScan(basePackages = {"com.candy"},excludeFilters = {
          @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = {Controller.class, Service.class}),
          @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,classes = {UserService.class})
  })
  ```

  FilterType.CUSTOM：自定义匹配(自定义一个类，实现接口TypeFilter)

  ```
  @ComponentScan(basePackages = {"com.candy"},excludeFilters = {
          @ComponentScan.Filter(type = FilterType.CUSTOM,classes = {MyFilterType.class})
  })
  ```

  ```
  import org.springframework.core.io.Resource;
  import org.springframework.core.type.AnnotationMetadata;
  import org.springframework.core.type.ClassMetadata;
  import org.springframework.core.type.classreading.MetadataReader;
  import org.springframework.core.type.classreading.MetadataReaderFactory;
  import org.springframework.core.type.filter.TypeFilter;
  
  import java.io.IOException;
  
  /**
   * @author Candy
   * @create 2021-04-08 12:39
   */
  public class MyFilterType implements TypeFilter {
  
      /**
       * 逐个扫描得到类的信息
       * @param metadataReader：读取到当前已经扫描到的类信息
       * @param metadataReaderFactory：可以获取到其他任何类的信息
       * 根据返回的Boolean来确定是否匹配到
       */
      @Override
      public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
          //获取当前扫描类的注解信息
          AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
          //获取当前扫描类的信息
          ClassMetadata classMetadata = metadataReader.getClassMetadata();
          //获取当前扫描类资源的信息(类的路径)
          Resource resource = metadataReader.getResource();
  
          String className = classMetadata.getClassName();
          System.out.println("当前类的名称"+className);
          //把当前类名称匹配包含Service的得到
          if(className.contains("Service")){
              return true;
          }
          return false;
      }
  }
  ```

  ![自定义包扫描类型](C:\Users\61788\Desktop\Spring注解驱动\自定义包扫描类型.png)

  FilterType.REGEX：正则表达式

- 包含扫描：includeFilters属性

  **必须设置useDefaultFilters = false**

  ```
  @ComponentScan(basePackages = {"com.candy"},useDefaultFilters = false,includeFilters = {
          @ComponentScan.Filter()
  })
  ```

#### 3.@Scope的作用

指定当前实例是否为单例

@Scope("singleton")：单例模式(默认) ,在IOC容器启动时候创建

@Scope("prototype")：多例模式，在使用的时候创建(多例模式，容器不会管理)

```
@Bean("us")
@Scope("prototype")
public User user(){
    return new User("张凯",29);
}
```

#### 4.使用@Lazy懒加载实例

在第一次获取的时候加载实例

```
@Bean("us")
@Lazy
public User user(){
    System.out.println("调用");
    return new User("张凯",29);
}
```



#### 5.按照条件向容器中注入Bean实例：@Conditional

**可以标注到方法或者类上**

```
@Conditional({UserCondition.class})
@Bean()
public User user01(){
    return new User("孙健",29);
}
```

**需要自定义类，实现接口Condition自定义条件**

```
import com.candy.bean.User;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author Candy
 * @create 2021-04-08 13:33
 */
public class UserCondition implements Condition {

    /**
     *
     * @param conditionContext：判断条件使用的上下文环境
     * @param annotatedTypeMetadata：当前标记了@Conditional注解得 注释信息
     */
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        //得到IOC的Bean创建工厂
        ConfigurableListableBeanFactory beanFactory = conditionContext.getBeanFactory();
        User user = beanFactory.getBean("us", User.class);
        //类加载器
        ClassLoader classLoader = conditionContext.getClassLoader();
        if(user.getAge() > 30){
            return true;
        }
        return false;
    }
}
```

#### 6.使用@Import快速添加Bean实例

##### 1.直接通过类型导入

```
@Import({Color.class})//可以导入多个
public class SpringConfig {
```

##### 2.可以自定义类，实现接口ImportSelector，根据自定义业务返回需要注册的Bean实例

```
@Import({Color.class,MyImportSelector.class})
public class SpringConfig {
```

```
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author Candy
 * @create 2021-04-08 18:16
 */
public class MyImportSelector implements ImportSelector {

    /**
     *
     * @param annotationMetadata：当前标注注解@Import类的所有注解信息
     * @return：返回一个数组，类的全类名
     */
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{"com.candy.bean.Blue"};
    }
}
```

#### 7.使用Spring提供的FactoryBean添加实例

**看getObject()方法返回的类来进行添加实例**

```
@Bean
public MyFactoryBean myFactoryBean(){
    return new MyFactoryBean();
}
```

```
import com.candy.bean.Menu;
import org.springframework.beans.factory.FactoryBean;

public class MyFactoryBean implements FactoryBean<Menu> {

    @Override
    public Menu getObject() throws Exception {
        return new Menu();
    }

    @Override
    public Class<?> getObjectType() {
        return Menu.class;
    }

    //是否单例模式创建实例
    @Override
    public boolean isSingleton() {
        return true;
    }
}
```

### Bean的生命周期

#### 1.自定义实例，通过@Bean指定初始化和销毁方法

- Bean的生命周期由容器管理

- 我们可以自定义初始化和销毁方法

  ```
  @Bean(value = "us",initMethod = "init",destroyMethod = "detory")
  public User user(){
      System.out.println("调用");
      return new User("张凯",29);
  }
  ```

  ```
  public class User {
      private String name;
      private Integer age;
  
      public User() {}
      public User(String name, Integer age) {
          this.name = name;
          this.age = age;
      }
  
      public void init(){
          System.out.println("init");
      }
  
      public void detory(){
          System.out.println("detory");
      }
  }
  ```

- 通过实现接口，InitializingBean, DisposableBean定义初始化和销毁方法

  ```
  import org.springframework.beans.factory.DisposableBean;
  import org.springframework.beans.factory.InitializingBean;
  
  public class Color implements InitializingBean, DisposableBean {
      @Override
      public void afterPropertiesSet() throws Exception {
          System.out.println("初始化方法===========");
      }
  
      @Override
      public void destroy() throws Exception {
          System.out.println("销毁方法===========");
      }
  }
  ```

**容器IOC销毁的时候才执行销毁方法(如果是多实例的情况，不会执行销毁方法)：IOC.close()**

#### 2.BeanPostProcessor后置处理器

- postProcessBeforeInitialization 

  在当前Bean任何初始化方法之前工作

  ```
  /**
   * @param bean  当前实例
   * @param beanName  实例名称
   */
  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
      System.out.println("postProcessBeforeInitialization--->实例："+bean+"名称："+beanName);
      return bean;
  }
  ```

- postProcessAfterInitialization

  在当前Bean所有初始化方法之后工作

  ```
  /**
   * @param bean  当前实例
   * @param beanName  实例名称
   */
  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
      System.out.println("postProcessAfterInitialization--->实例："+bean+"名称："+beanName);
      return bean;
  }
  ```



### 通过接口ApplicationContextAware得到IOC容器

```
public class ManTest implements ApplicationContextAware {

    private ApplicationContext IOC;
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.IOC = applicationContext;
    }
```



## 使用@Value注解向容器中的Bean赋值

1.导入配置文件

```
import com.candy.bean.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Candy
 * @create 2021-04-09 13:02
 */
@Configuration
@Import({User.class})
@PropertySource(value = {"classpath:/jdbc.properties"})//读取配置文件,使用@Value("${url}")前提
public class SpringConfigValues {
}
```

```
/**
 * 1.可以直接赋值
 * 2.可是使用SPEL赋值：#{20-18}
 * 3.取出配置文件中的值：${KEY}(需要再配置类中先导入配置文件)
 */
@Value("孙健")
private String name;
@Value("${url}")
private String neckName;
@Value("#{20-18}")
private Integer age;
```

2.使用@Value("${url}")读取配置文件

```
/**
 * 1.可以直接赋值
 * 2.可是使用SPEL赋值：#{20-18}
 * 3.取出配置文件中的值：${KEY}(需要再配置类中先导入配置文件)
 */
@Value("孙健")
private String name;
@Value("${url}")
private String neckName;
@Value("#{20-18}")
private Integer age;
```

3.从IOC容器中获取配置文件中的值

```
@SpringJUnitConfig(classes = SpringConfigValues.class)//加载配置文件
public class SpringConfigValuesTest implements ApplicationContextAware {

    private ApplicationContext IOC;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.IOC = applicationContext;
    }

	@Test
    public void test(){
        Environment environment = IOC.getEnvironment();
        String url = environment.getProperty("url");
        System.out.println("读取properties配置文件中的值："+url);
    }
```

## 自动装配

- @Autowired：根据类型自动装配，

- @Qualifier("userServiceImpl2")：根据名称自动装配，需要与@Autowired搭配使用

  ```
  @Autowired
  @Qualifier("XXX")
  private UserService UserService;
  ```

- @Resource：通过名称自动装配，如果多个实例，需要指定名称(不推荐使用)

  ```
  @Resource(name = "userServiceImpl2")
  private UserService UserService;
  ```



## AOP

**(com.candy.aop下的代码)**

### 1.导入AOP模块依赖

```
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-aspects</artifactId>
    <version>Spring版本</version>
</dependency>
<dependency>
    <groupId>net.sourceforge.cglib</groupId>
    <artifactId>com.springsource.net.sf.cglib</artifactId>
    <version>2.2.0</version>
</dependency>
<dependency>
    <groupId>org.aopalliance</groupId>
    <artifactId>com.springsource.org.aopalliance</artifactId>
    <version>1.0.0</version>
</dependency>
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>com.springsource.org.aspectj.weaver</artifactId>
    <version>1.6.4.RELEASE</version>
</dependency>
```

### 2.定义增强切面类

- 指定切面类由Spring管理，并且标注此类是一个切面类Aspect

- 指定切入点

- 定义增强方法业务

  ​	前置增强(@Before)：目标业务运行之前执行的增强

  ​	后置增强(@AfterReturning)：目标业务运行之后，如果出现异常，则不执行的增强

  ​	最终增强(@After)：目标业务之后，无论是否有异常，一定执行的增强

  ​	异常增强(@AfterThrowing)：目标业务异常执行的增强

  ​	环绕增强(@Around)：目标业务执行前后的增强，如果出现异常，则不执行环绕后置增强

![AOP](C:\Users\61788\Desktop\Spring注解驱动\AOP增强执行顺序.png)



```
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author Candy
 * @create 2021-04-12 15:05
 * 日志切面
 * 前置增强(@Before)：logBefore()
 * 最终增强(@After)：logAfter(),无论方法正常结束，还是异常结束都一定调用
 * 后置增强(@AfterReturning)：logAfterReturning(),如果出现异常，则不会调用
 * 异常增强(@AfterThrowing)：logException()
 * 环绕增强(@Around)：手动推进方法运行
 * ***
 *      JoinPoint joinPoint必须出现在参数的第一位
 */
@Aspect
@Component
public class LogAspects {

    @Pointcut("execution(* com.candy.aop.service..*.*(..))")
    public void pointLog(){}

    @Before("com.candy.aop.config.LogAspects.pointLog()")
    public void logBefore(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();//参数列表
        System.out.println("方法"+joinPoint.getSignature().getName()+"运行之前....参数列表是：{"+ Arrays.asList(args) +"}");
    }
    @After("pointLog()")
    public void logAfter(JoinPoint joinPoint){
        System.out.println("方法"+joinPoint.getSignature().getName()+"最终增强一定调用....");
    }
    @AfterReturning(value = "pointLog()",returning = "obj")
    public void logAfterReturning(JoinPoint joinPoint,Object obj){
        System.out.println("方法"+joinPoint.getSignature().getName()+"正常运行之后....运行返回参数是：{"+obj+"}");
    }
    @AfterThrowing(value = "pointLog()",throwing = "e")
    public void logException(JoinPoint joinPoint,Exception e){
        System.out.println("方法"+joinPoint.getSignature().getName()+"异常....异常信息：{"+e+"}");
    }
    @Around("pointLog()")
    public void Around(ProceedingJoinPoint joinPoint)throws Throwable{
        System.out.println("环绕之前");
        joinPoint.proceed();
        System.out.println("环绕之后");
    }
}
```

### 3.Spring配置类开启AOP

**使用注解：@EnableAspectJAutoProxy开启AOP功能**

因为Spring实现动态代理有两种实现方法，一个基于JDK实现，一个基于CGLIB；

- ​	如果业务类没有实现接口，则使用CGLIB实现动态代理；
- ​	如果业务有实现接口，则使用JDK实现动态代理；

**如果需要让Spring强制使用CGLIB实现动态代理，则需要加：proxyTargetClass = true**

```
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan(basePackages = {"com.candy.aop"})
@EnableAspectJAutoProxy(proxyTargetClass = true)//开启AOP功能，并且之前无论何时都使用CGLIB实现动态代理
public class SpringAOPConfig {
}
```

## 声明式事务TX

### 1.引入依赖

```
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-tx</artifactId>
    <version>Spring版本</version>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-jdbc</artifactId>
    <version>Spring版本</version>
</dependency>

<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.2.3</version>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.49</version>
</dependency>
```



### 2.开启注解事务驱动和事务管理器

- 1.在Spring配置类中添加：@EnableTransactionManagement(proxyTargetClass = true)

- 2.配置事务管理器(基于JdbcTemplate或者Mybatis)

  ```
  //事务管理器
  @Bean
  public DataSourceTransactionManager transactionManager(DataSource dataSource){
      return new DataSourceTransactionManager(dataSource);
  }
  ```

### 3.使用事务得方式

方式一：使用注解@Transactional(可以查看Spring5得事务笔记)

## 注解版整合SpringMVC

### 1.导入依赖

```
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>Spring版本</version>
</dependency>
<!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.1.0</version>
    <scope>provided</scope>
</dependency>
```

### 2.自定义配置类MyWebAppInitializer继承AbstractAnnotationConfigDispatcherServletInitializer

```
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {


    //获取父容器的配置类(Spring配置文件)
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{XXX.class};
    }

    //获取web子容器的配置类(SpringMVC配置文件)
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{XXX.class};
    }

    /**
     * 配置DispatcherServlet请求映射信息:
     * /：拦截所有请求，包括静态资源，但是不包括*.jsp
     * /*：拦截所有请求，包括静态资源，还包括*.jsp
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
```



### 3.编写spring父容器和SpringMVC子容器

```
@ComponentScan(value = "com.candy.springmvc",
excludeFilters = {
    @ComponentScan.Filter(
        type = FilterType.ANNOTATION,classes = Controller.class
    )
})//包扫描排除了Controller,为了和web容器，形成父子容器
public class SpringConfig {
}
```

```
@ComponentScan(value = "com.candy.springmvc",useDefaultFilters = false,
includeFilters = {
    @ComponentScan.Filter(
        type = FilterType.ANNOTATION,classes = Controller.class
    )
})//包扫描只包含Controller,为了和web容器，形成父子容器
public class SpringmvcConfig {
}
```

### 4.自定义SpringMVC的配置

```
@EnableWebMvc
@ComponentScan(value = "com.candy.springmvc",useDefaultFilters = false,
includeFilters = {
    @ComponentScan.Filter(
        type = FilterType.ANNOTATION,classes = Controller.class
    )
})//包扫描只包含Controller,为了和web容器，形成父子容器
public class SpringmvcConfig implements WebMvcConfigurer {
    //配置视图解析器
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/views/", ".jsp");
    }
}
```

1. **使用注解@EnableWebMvc开启自定义配置功能**

   相当于以前的XML配置

   ```
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
           http://www.springframework.org/schema/beans
           https://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/mvc
           https://www.springframework.org/schema/mvc/spring-mvc.xsd">
   
       <mvc:annotation-driven/>
   </beans>
   ```

2. **实现接口WebMvcConfigurer自定义MVC配置**

   相当于以前XML配置

   ```
   <!-- 视图解析器 -->
   <bean id="viewResolver"
       class="org.springframework.web.servlet.view.InternalResourceViewResolver">
       <property name="prefix" value="/WEB-INF/view/"></property>
       <property name="suffix" value=".jsp"></property>
   </bean>
   ```

3. **配置静态资源访问**

   相当于以前XML配置

   ```
   <?xml version="1.0" encoding="UTF-8"?>
   <beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
           http://www.springframework.org/schema/beans
           https://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/mvc
           https://www.springframework.org/schema/mvc/spring-mvc.xsd">
   
       <mvc:default-servlet-handler />
   </beans>
   ```

4. **拦截器配置**

   相当于以前XML配置

   ```
   <mvc:interceptors>
       <bean class="org.springframework.web.servlet.i18n.LocaleChangeInterceptor"/>
       <mvc:interceptor>
           <mvc:mapping path="/**"/>
           <mvc:exclude-mapping path="/admin/**"/>
           <bean class="org.springframework.web.servlet.theme.ThemeChangeInterceptor"/>
       </mvc:interceptor>
       <mvc:interceptor>
           <mvc:mapping path="/secure/*"/>
           <bean class="org.example.SecurityInterceptor"/>
       </mvc:interceptor>
   </mvc:interceptors>
   ```

   自定义拦截器配置，需要实现接口HandlerInterceptor

   ```
   import org.springframework.web.servlet.HandlerInterceptor;
   import org.springframework.web.servlet.ModelAndView;
   
   import javax.servlet.http.HttpServletRequest;
   import javax.servlet.http.HttpServletResponse;
   
   public class MyInterceptor implements HandlerInterceptor {
   
       //目标方法之前执行,返回true放行
       @Override
       public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
           System.out.println("preHandle...");
           return true;
       }
   
       //目标方法运行之后执行
       @Override
       public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
           System.out.println("postHandle...");
       }
   
       //页面响应后执行
       @Override
       public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
           System.out.println("afterCompletion...");
       }
   }
   ```



