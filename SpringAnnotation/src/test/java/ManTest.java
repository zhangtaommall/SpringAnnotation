import com.candy.bean.User;
import com.candy.config.spring.SpringConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Map;

/**
 * @author Candy
 * @create 2021-04-07 17:00
 */
@SpringJUnitConfig(classes = SpringConfig.class)//加载配置文件
public class ManTest implements ApplicationContextAware {

    private ApplicationContext IOC;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.IOC = applicationContext;
    }


    @Autowired
    @Qualifier("us")
    private User user;

    @Test
    public void test(){
//        String[] beanDefinitionNames = IOC.getBeanDefinitionNames();
//        for (String beanDefinitionName : beanDefinitionNames) {
//            System.out.println(beanDefinitionName);
//        }
        User us = (User) IOC.getBean("us");
        User us2 = (User) IOC.getBean("us");
//        Object myFactoryBean = IOC.getBean("myFactoryBean");
        System.out.println(us);
        System.out.println(us2);
//        System.out.println(myFactoryBean.getClass());
//        IOC.close();
    }

    @Test
    public void test2(){
        String[] beanNamesForType = IOC.getBeanNamesForType(User.class);
        for (String s : beanNamesForType) {
            System.out.println(s);
        }

        Map<String, User> beansOfType = IOC.getBeansOfType(User.class);
        System.out.println(beansOfType);

    }

}
