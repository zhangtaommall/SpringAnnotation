package aop;

import com.candy.aop.config.SpringAOPConfig;
import com.candy.aop.service.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.sql.DataSource;

/**
 * @author Candy
 * @create 2021-04-12 15:36
 */
@SpringJUnitConfig(classes = SpringAOPConfig.class)//加载配置文件
public class AOPTest {

    @Autowired
    private PersonService personService;

    @Test
    public void test(){
        System.out.println(personService);
        Integer div = personService.div(10, 2);
        System.out.println("test...."+div);
    }


    //输出容器中所有的组件Bean
    private void printBean(ApplicationContext IOC){
        String[] beanDefinitionNames = IOC.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
    }
}
