import com.candy.bean.Blue;
import com.candy.bean.User;
import com.candy.config.spring.SpringConfig2;
import com.candy.dao.UserDao;
import com.candy.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.annotation.Resource;

/**
 * @author Candy
 * @create 2021-04-09 13:03
 */
@SpringJUnitConfig(classes = SpringConfig2.class)//加载配置文件
public class SpringConfig2Test implements ApplicationContextAware {

    private ApplicationContext IOC;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.IOC = applicationContext;
    }

    @Autowired
    private Blue blue;
    @Autowired
    private UserDao userDao;

    @Resource(name = "userServiceImpl2")
    private UserService UserService;

    @Test
    public void test2(){
        System.out.println(userDao);
    }

    @Test
    public void test(){
        printBean(IOC);
        User bean = IOC.getBean("com.candy.bean.User", User.class);
        System.out.println(bean);

        Environment environment = IOC.getEnvironment();
        String url = environment.getProperty("url");
        System.out.println("读取properties配置文件中的值："+url);
    }

    //输出容器中所有的组件Bean
    private void printBean(ApplicationContext IOC){
        String[] beanDefinitionNames = IOC.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
    }

}
