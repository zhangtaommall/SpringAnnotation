package listener;

import com.candy.listener.ListenerService;
import com.candy.listener.SpringListenerConfig;
import com.candy.tx.bean.Account;
import com.candy.tx.config.SpringTxConfig;
import com.candy.tx.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

/**
 * @author Candy
 * @create 2021-04-13 11:11
 */
@SpringJUnitConfig(classes = SpringListenerConfig.class)//加载配置文件
public class ListenerTest {

    @Autowired
    private ApplicationContext application;

    @Test
    public void test(){
        application.publishEvent(new ApplicationEvent("发布事件"){});
        ListenerService listenerService = application.getBean("listenerService", ListenerService.class);

        System.out.println(listenerService);
    }

}
