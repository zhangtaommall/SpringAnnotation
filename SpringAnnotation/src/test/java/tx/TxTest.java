package tx;

import com.candy.tx.config.SpringTxConfig;
import com.candy.tx.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.sql.DataSource;

/**
 * @author Candy
 * @create 2021-04-13 11:11
 */
@SpringJUnitConfig(classes = SpringTxConfig.class)//加载配置文件
public class TxTest {

    @Autowired
    private UserService userService;
    @Autowired
    private DataSource dataSource;

    @Test
    public void test(){
        System.out.println(dataSource);
//        userService.add(new Account("子剑2",1000));
    }

}
