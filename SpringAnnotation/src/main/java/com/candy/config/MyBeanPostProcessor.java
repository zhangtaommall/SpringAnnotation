package com.candy.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author Candy
 * @create 2021-04-09 11:45
 */
public class MyBeanPostProcessor implements BeanPostProcessor {

    /**
     * @param bean  当前实例
     * @param beanName  实例名称
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessBeforeInitialization--->实例："+bean+"名称："+beanName);
        return bean;
    }

    /**
     * @param bean  当前实例
     * @param beanName  实例名称
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization--->实例："+bean+"名称："+beanName);
        return bean;
    }
}
