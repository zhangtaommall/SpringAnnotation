package com.candy.config.spring;

import com.candy.bean.User;
import com.candy.config.MyFactoryBean;
import com.candy.dao.UserDao;
import org.springframework.context.annotation.*;

/**
 * @author Candy
 * @create 2021-04-09 13:02
 */
@Configuration
@ComponentScan(basePackages = {"com.candy"})
@Import({User.class})
@PropertySource(value = {"classpath:/jdbc.properties"})//读取配置文件,使用@Value("${url}")前提
public class SpringConfig2 {

    @Bean("userDao2")
    public UserDao userDao(){
        UserDao userDao = new UserDao();
        userDao.setName("userDao2");
        return userDao;
    }
}
