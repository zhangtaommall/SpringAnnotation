package com.candy.config.spring;

import com.candy.bean.Color;
import com.candy.bean.Menu;
import com.candy.bean.User;
import com.candy.config.*;
import com.candy.service.UserServiceImpl;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * @author Candy
 * @create 2021-04-07 17:18
 */
@Configuration//相当于XML配置文件
@ComponentScan(basePackages = {"com.candy"},excludeFilters = {
        @ComponentScan.Filter(type = FilterType.CUSTOM,classes = {MyFilterType.class})
})
//@ComponentScan(basePackages = {"com.candy"},useDefaultFilters = false,includeFilters = {
//        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = {Service.class})
//})
@Import({Color.class, MyImportSelector.class, MyBeanPostProcessor.class})
public class SpringConfig{

    /**
     * 向容器中注册一个Bean实例；类型是返回值的类型；
     * id默认是方法名字,或者使用@Bean("XXX")指定id名称
     */
    @Bean(value = "us",initMethod = "init",destroyMethod = "detory")
    @Lazy
    public User user(){
        System.out.println("调用");
        return new User("张凯",29);
    }
    @Bean
    public MyFactoryBean myFactoryBean(){
        return new MyFactoryBean();
    }

    @Conditional({UserCondition.class})
    @Bean()
    public User user01(){
        return new User("孙健",29);
    }

    @Conditional({UserCondition.class})
    @Bean()
    public User user02(){
        return new User("孙健2",31);
    }

}
