package com.candy.config;

import com.candy.bean.User;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author Candy
 * @create 2021-04-08 13:33
 */
public class UserCondition implements Condition {

    /**
     *
     * @param conditionContext：判断条件使用的上下文环境
     * @param annotatedTypeMetadata：当前标记了@Conditional注解得 注释信息
     */
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        //得到IOC的Bean创建工厂
        ConfigurableListableBeanFactory beanFactory = conditionContext.getBeanFactory();
        User user = beanFactory.getBean("us", User.class);
        //类加载器
        ClassLoader classLoader = conditionContext.getClassLoader();
        if(user.getAge() > 30){
            return true;
        }
        return false;
    }
}
