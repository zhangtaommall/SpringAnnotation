package com.candy.config;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

/**
 * @author Candy
 * @create 2021-04-08 12:39
 */
public class MyFilterType implements TypeFilter {

    /**
     * 逐个扫描得到类的信息
     * @param metadataReader：读取到当前已经扫描到的类信息
     * @param metadataReaderFactory：可以获取到其他任何类的信息
     * 根据返回的Boolean来确定是否匹配到
     */
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        //获取当前扫描类的注解信息
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
        //获取当前扫描类的信息
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        //获取当前扫描类资源的信息(类的路径)
        Resource resource = metadataReader.getResource();

        String className = classMetadata.getClassName();
        System.out.println("当前类的名称"+className);
        //把当前类名称匹配包含Service的得到
        if(className.contains("Service")){
            return true;
        }
        return false;
    }
}
