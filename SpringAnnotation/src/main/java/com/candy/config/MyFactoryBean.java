package com.candy.config;

import com.candy.bean.Menu;
import org.springframework.beans.factory.FactoryBean;

public class MyFactoryBean implements FactoryBean<Menu> {

    @Override
    public Menu getObject() throws Exception {
        return new Menu();
    }

    @Override
    public Class<?> getObjectType() {
        return Menu.class;
    }

    //是否单例模式创建实例
    @Override
    public boolean isSingleton() {
        return true;
    }
}
