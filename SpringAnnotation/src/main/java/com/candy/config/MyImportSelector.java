package com.candy.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author Candy
 * @create 2021-04-08 18:16
 */
public class MyImportSelector implements ImportSelector {

    /**
     *
     * @param annotationMetadata：当前标注注解@Import类的所有注解信息
     * @return：返回一个数组，类的全类名
     */
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{"com.candy.bean.Blue"};
    }
}
