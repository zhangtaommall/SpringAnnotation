package com.candy.tx.service;

import com.candy.tx.bean.Account;

/**
 * @author Candy
 * @create 2021-04-13 10:57
 */
public interface UserService {

    void add(Account account);

}
