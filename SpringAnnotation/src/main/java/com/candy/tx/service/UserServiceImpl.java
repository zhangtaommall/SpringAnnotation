package com.candy.tx.service;

import com.candy.tx.bean.Account;
import com.candy.tx.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

/**
 * @author Candy
 * @create 2021-04-13 10:58
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional()
    public void add(Account account) {
        userDao.add(account);
        int i = 10 / 0;
        System.out.println("插入完成");
    }
}
