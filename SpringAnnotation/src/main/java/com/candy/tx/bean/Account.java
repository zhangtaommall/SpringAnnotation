package com.candy.tx.bean;

import lombok.Data;

/**
 * @author Candy
 * @create 2021-04-13 11:01
 */
@Data
public class Account {

    private String userName;
    private Integer money;

    public Account() {}

    public Account(String userName, Integer money) {
        this.userName = userName;
        this.money = money;
    }
}
