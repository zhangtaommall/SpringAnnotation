package com.candy.tx.dao;

import com.candy.tx.bean.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author Candy
 * @create 2021-04-13 10:59
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(Account account) {
        String sql = "insert into `account`(user_name,money) values(?,?);";
        int update = jdbcTemplate.update(sql,account.getUserName(),account.getMoney());
        System.out.println(update);
    }
}
