package com.candy.tx.dao;

import com.candy.tx.bean.Account;

/**
 * @author Candy
 * @create 2021-04-13 10:58
 */
public interface UserDao {

    void add(Account account);
}
