package com.candy.tx.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Candy
 * @create 2021-04-12 19:57
 */
@Configuration
@ComponentScan(basePackages = {"com.candy.tx"})
@EnableTransactionManagement(proxyTargetClass = true)
public class SpringTxConfig {
}
