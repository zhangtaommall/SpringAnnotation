package com.candy.dao;

import org.springframework.stereotype.Repository;

/**
 * @author Candy
 * @create 2021-04-07 18:11
 */
@Repository
public class UserDao {

    private String name = "aa";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
