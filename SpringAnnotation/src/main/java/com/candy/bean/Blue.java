package com.candy.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Candy
 * @create 2021-04-08 18:19
 */
@Component
public class Blue {

    private Color color;

    public Blue(Color color) {
        System.out.println("======Blue=====");
        this.color = color;
    }
}
