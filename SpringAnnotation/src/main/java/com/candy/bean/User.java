package com.candy.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Candy
 * @create 2021-04-07 16:56
 */
@Data
public class User {

    /**
     * 1.可以直接赋值
     * 2.可是使用SPEL赋值：#{20-18}
     * 3.取出配置文件中的值：${KEY}(需要再配置类中先导入配置文件)
     */
    @Value("孙健")
    private String name;
    @Value("${url}")
    private String neckName;
    @Value("#{20-18}")
    private Integer age;

    public User() {}
    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public void init(){
        System.out.println("init");
    }

    public void detory(){
        System.out.println("detory");
    }
}
