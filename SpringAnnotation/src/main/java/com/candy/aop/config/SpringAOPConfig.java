package com.candy.aop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan(basePackages = {"com.candy.aop"})
@EnableAspectJAutoProxy(proxyTargetClass = true)//开启AOP功能，并且之前无论何时都使用CGLIB实现动态代理
public class SpringAOPConfig {
}
