package com.candy.aop.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author Candy
 * @create 2021-04-12 15:05
 * 日志切面
 * 前置增强(@Before)：logBefore()
 * 最终增强(@After)：logAfter(),无论方法正常结束，还是异常结束都一定调用
 * 后置增强(@AfterReturning)：logAfterReturning(),如果出现异常，则不会调用
 * 异常增强(@AfterThrowing)：logException()
 * 环绕增强(@Around)：手动推进方法运行
 * ***
 *      JoinPoint joinPoint必须出现在参数的第一位
 */
@Aspect
@Component
public class LogAspects {

    @Pointcut("execution(* com.candy.aop.service..*.*(..))")
    public void pointLog(){}

    @Before("com.candy.aop.config.LogAspects.pointLog()")
    public void logBefore(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();//参数列表
        System.out.println("方法"+joinPoint.getSignature().getName()+"运行之前....参数列表是：{"+ Arrays.asList(args) +"}");
    }
    @After("pointLog()")
    public void logAfter(JoinPoint joinPoint){
        System.out.println("方法"+joinPoint.getSignature().getName()+"最终增强一定调用....");
    }
    @AfterReturning(value = "pointLog()",returning = "obj")
    public void logAfterReturning(JoinPoint joinPoint,Object obj){
        System.out.println("方法"+joinPoint.getSignature().getName()+"正常运行之后....运行返回参数是：{"+obj+"}");
    }
    @AfterThrowing(value = "pointLog()",throwing = "e")
    public void logException(JoinPoint joinPoint,Exception e){
        System.out.println("方法"+joinPoint.getSignature().getName()+"异常....异常信息：{"+e+"}");
    }
    @Around("pointLog()")
    public void Around(ProceedingJoinPoint joinPoint)throws Throwable{
        System.out.println("环绕之前");
        joinPoint.proceed();
        System.out.println("环绕之后");
    }
}
