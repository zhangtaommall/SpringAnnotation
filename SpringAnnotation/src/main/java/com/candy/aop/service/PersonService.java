package com.candy.aop.service;

/**
 * @author Candy
 * @create 2021-04-12 15:05
 */
public interface PersonService {

    Integer div(int i,int j);
    Integer add(int i,int j);
}
