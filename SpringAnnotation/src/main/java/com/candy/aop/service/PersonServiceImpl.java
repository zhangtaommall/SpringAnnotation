package com.candy.aop.service;

import org.springframework.stereotype.Service;

/**
 * @author Candy
 * @create 2021-04-12 15:09
 */
@Service
public class PersonServiceImpl implements PersonService{
    @Override
    public Integer div(int i,int j) {
        System.out.println("PersonServiceImpl....div....");
        add(i,j);
        return i/j;
    }

    @Override
    public Integer add(int i, int j) {
        System.out.println("PersonServiceImpl....add....");
        return i+j;
    }
}
