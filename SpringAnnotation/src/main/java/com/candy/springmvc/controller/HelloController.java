package com.candy.springmvc.controller;

import com.candy.springmvc.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Candy
 * @create 2021-04-14 16:55
 */
@Controller
public class HelloController {

    @Autowired
    private HelloService helloService;

    @ResponseBody
    @RequestMapping("hello")
    public String hello(){
        String hello = helloService.hello("tomcat...");
        return hello;
    }

    @RequestMapping("dss")
    public String index(){
        return "index";
    }
}
