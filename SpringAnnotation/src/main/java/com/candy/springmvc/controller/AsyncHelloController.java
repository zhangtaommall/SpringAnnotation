package com.candy.springmvc.controller;

import com.candy.springmvc.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.Callable;

/**
 * @author Candy
 * @create 2021-04-15 11:30
 */
@Controller
public class AsyncHelloController {


    @Autowired
    private HelloService helloService;

    /**
     * 1.返回Callable,SpringMVC将Callable提交到TaskExecutor另一个隔离线程池执行
     * 2.DispatcherServlet 和所有的filter退出web容器线程；但是response 依然保留打开
     * 3.Callable 返回结果，SpringMVC将请求重新派发到容器，重新处理之前的返回处理(相当于请求再次发送到MVC重新处理)
     * 最能证明请求被SpringMVC再次重新处理，可以查看拦截器的preHandle被触发2次
     */
    @RequestMapping("/asyncHello")
    @ResponseBody
    public Callable<String> hello() {
        Callable callable = new Callable<String>(){
            @Override
            public String call() throws Exception {
                return helloService.hello("tomcat...");
            }
        };
        return callable;
    }

}
