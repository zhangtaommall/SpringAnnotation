package com.candy.springmvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;

/**
 * @author Candy
 * @create 2021-04-14 16:46
 * web子容器
 * ****useDefaultFilters = false一定要写，禁用默认过滤规则
 */
@EnableWebMvc
@ComponentScan(value = "com.candy.springmvc",useDefaultFilters = false,
includeFilters = {
    @ComponentScan.Filter(
        type = FilterType.ANNOTATION,classes = Controller.class
    )
})//包扫描只包含Controller,为了和web容器，形成父子容器
public class SpringmvcConfig implements WebMvcConfigurer {

    //配置视图解析器
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/views/", ".jsp");
    }

    //静态资源访问
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    //拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptor()).addPathPatterns("/**").excludePathPatterns("/admin/**");
    }



}
