package com.candy.springmvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * @author Candy
 * @create 2021-04-14 16:46
 * Spring父容器
 */
@ComponentScan(value = "com.candy.springmvc",
excludeFilters = {
    @ComponentScan.Filter(
        type = FilterType.ANNOTATION,classes = Controller.class
    )
})//包扫描排除了Controller,为了和web容器，形成父子容器
public class SpringConfig {
}
