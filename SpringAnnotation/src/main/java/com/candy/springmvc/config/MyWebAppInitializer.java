package com.candy.springmvc.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {


    //获取父容器的配置类(Spring配置文件)
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{SpringConfig.class};
    }

    //获取web子容器的配置类(SpringMVC配置文件)
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{SpringmvcConfig.class};
    }

    /**
     * 配置DispatcherServlet请求映射信息:
     * /：拦截所有请求，包括静态资源，但是不包括*.jsp
     * /*：拦截所有请求，包括静态资源，还包括*.jsp
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
