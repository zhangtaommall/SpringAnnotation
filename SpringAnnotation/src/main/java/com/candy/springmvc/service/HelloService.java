package com.candy.springmvc.service;

/**
 * @author Candy
 * @create 2021-04-14 16:56
 */
public interface HelloService {

    String hello(String name);
}
