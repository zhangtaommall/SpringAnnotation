package com.candy.springmvc.service;

import org.springframework.stereotype.Service;

/**
 * @author Candy
 * @create 2021-04-14 16:57
 */
@Service
public class HelloServiceImpl implements HelloService {


    @Override
    public String hello(String name) {
        return "HelloService...."+name;
    }
}
