package com.candy.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * @author Candy
 * @create 2021-04-13 14:49
 */
@Service
public class ListenerService {

    @EventListener(classes = {ApplicationEvent.class})
    public void listener(ApplicationEvent event){
        System.out.println("listener收到事件--------->"+event);
    }

}
